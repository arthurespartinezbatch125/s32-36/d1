//we will use this for authentication
	//login
	//retrieve user detail

//Json Web Token
	//methods:
		//sign(data, secret, {options}) - creates token, alphanumeric characters
		//verify(token, secret, cb()) - bcheck if token is present
		//decode(token, {options}) - interpret/decodes the created token
let jwt = require('jsonwebtoken');
let secret = "CourseBookingAPI";

//create a token

module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {});
}

//verify token
module.exports.verify = (req, res, next) =>{
	let token = req.headers.authorization //authorization holds the token

	// console.log(token); //bearer dhieiejieij

	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data)=>{
			if(error){
				return res.send({auth: "failed"})
			} else {
				next();
			}
		})
	}
}
//decode token
module.exports.decode = (token) => {

	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
			if(err){
				return null
			} else {
				return jwt.decode(token, {complete: true}).payload
			}
		})
	} 
}
// decode(req.headers.authorization)