const express = require("express");
const router = express.Router();
const courseController = require('./../controllers/courseControllers');
let auth = require('./../auth')
//retrieve active courses 
router.get('/active', (req, res) => {
	courseController.getAllActive().then(result => res.send(result));
})
//retrieve all courses
router.get('/all', auth.verify, (req,res)=>{
	courseController.getAllCourses().then(result => res.send(result))
})
//add course
router.post('/addCourse', auth.verify, (req, res)=>{
	courseController.addCourse(req.body).then( result => res.send(result));
})
//make a route to get single course
router.get('/:courseId', auth.verify, (req, res) => {
	courseController.getSingleCourse(req.params).then(result => res.send(result))
})
//make a route update the course
router.put('/:courseId/edit', auth.verify, (req, res) => {
	courseController.editCourse(req.params.courseId, req.body).then(result => res.send(result))
})
//make a route to archive the course
	//meaning update the status of the course from true to false
router.put('/:courseId/archive', auth.verify, (req, res) => {
	courseController.archiveCourse(req.params.courseId).then(result => res.send(result))
})
//make a route to unarchive the course
	//update the status of the course from false to true
router.put('/:courseId/unarchive', auth.verify, (req, res) => {
	courseController.unarchiveCourse(req.params.courseId).then(result => res.send(result))
})
//make a route to delete a course
	//meaning the course object will be deleted from the database
router.delete('/:courseId/delete', auth.verify, (req, res) => {
	courseController.deleteCourse(req.params.courseId).then(result => res.send(result))
})
module.exports = router;