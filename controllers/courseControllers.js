const Course = require('./../models/courses')

module.exports.getAllActive = () => {
	//Model.method
	return Course.find({isActive: true}).then(result=>result)
}
module.exports.getAllCourses = () => {
	return Course.find().then(result=>result)
}
module.exports.addCourse =(reqBody) => {

	let newCourse = new Course ({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		})
	//Model.prototype.method
	return newCourse.save().then( (course, error) =>{
		if(error){
			return false
		} else {
			return true
		}
	})
}
module.exports.getSingleCourse = (params) => {
	return Course.findById(params.courseId).then(result => result)
}
//edit course
module.exports.editCourse = (params, reqBody) => {

	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	//Model.method
	return Course.findByIdAndUpdate(params, updatedCourse, {new: true})
	.then((result, error) => {
		if(error){
			return error
		} else {
			return result
		}
	})
}
module.exports.archiveCourse = (params) => {
	let updatedActiveCourse = {
		isActive: false
	}
	return Course.findByIdAndUpdate(params, updatedActiveCourse, {new: true})
	.then((result, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}
module.exports.unarchiveCourse = (params) => {
	let updatedActiveCourse = {
		isActive: true
	}
	return Course.findByIdAndUpdate(params, updatedActiveCourse, {new: true})
	.then((result, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}
module.exports.deleteCourse = (params) => {
	return Course.findByIdAndDelete(params)
	.then((result, error)=> {
		if(error){
			return false
		} else {
			return true
		}
	})
}